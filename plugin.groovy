import com.intellij.ide.BrowserUtil
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.progress.ProgressIndicator
import groovy.json.JsonSlurper

import static liveplugin.PluginUtil.registerAction
import static liveplugin.PluginUtil.show
import static liveplugin.PluginUtil.showPopupSearch

registerAction("ThesaurusSynonym", "ctrl alt shift Y") { AnActionEvent event ->
    def itemProvider = { String pattern, ProgressIndicator indicator ->
        thesaurusSynonymsFor(pattern)
    } as Object
    showPopupSearch("Synonyms List", event.project, itemProvider) { String item ->
        def term = item.tokenize('/')[0]
        BrowserUtil.open("https://www.dictionary.com/browse/${term}")
    }
}

static List<String> thesaurusSynonymsFor(String text) {
    text = URLEncoder.encode(text, "UTF-8")
    def json = "https://tuna.thesaurus.com/pageData/$text".toURL().text

    def result =  new JsonSlurper().parseText(json)

    if (! result.data) {
        return ["Nothing is found"]
    }

    def synonyms = []

    result.data.definitionData.definitions[0].synonyms.each {
        synonyms << "${it.term}/similarity:${it.similarity}/isInformal:${it.isInformal}/isVulgar:${it.isVulgar}"
    }

    return synonyms
}

if (!isIdeStartup) show("Loaded ThesaurusSynonyms<br/>Use ctrl+alt+shift+Y to run it")